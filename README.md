# Bitergia Panel Collections

This project contains collections of panels created by [Bitergia](https://bitergia.com) for [Bitergia Analytics](https://bitergia.com/products/dashboards/) and [GrimoireLab](https://chaoss.github.io/grimoirelab) related to three specific use cases: 
* Open Source Program Offices, 
* Inner Source Program Offices 
* Open Source Software Projects (mostly Open Source Foundations).

Panels documentation is available in [this project's website](https://bitergia.gitlab.io/panel-collections).

## How to build this project's website

This project's website is done with [Jekyll](https://jekyllrb.com/), and to build it locally you need to run, at least:

```
$ bundle install
```

And then:
```
$ bundle exec jekyll build
```
to build it locally, or to serve from your machine:
```
$ bundle exec jekyll serve
```

We recommend to read [Jekyll docs](https://jekyllrb.com/docs/) to know more about its options.

## How to contribute to this project

If you find a bug, or you have any question, or comment, feel free to [create an issue](https://gitlab.com/Bitergia/panel-collections/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) and we'll work on it as soon as possible.

If you want to submit new panels to existing collections, or to create a new collection under this project, please, read the [existing documentation](CONTRIBUTING.md).

## License

GPL v3