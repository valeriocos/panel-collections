---
title: Sample Documentation File
description: includes info about a given panel.
screenshot: sample_file.jpg
author: John Doe
created_at: 2018-12-05 (YYYY-MM-DD)
grimoirelab_version: x.x.x
layout: panel
---

## Basic Guidelines

Here you should cover, at least:
* What does it display (list of metrics)?
* Why was it built and what is it used for?
* Screenshot.

## Another section

[Sample internal link](#basic-guidelines)
