# How to contribute

This project contains a set of panels collections to be used by Bitergia Analytics and [GrimoireLab](https://chaoss.github.io/grimoirelab) platforms.

Besides [open issues](https://gitlab.com/Bitergia/panel-collections/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) about existing panels, there are 2 main ways to contribute to this project:

1. submitting new panels to existing collections
2. creating a new collection under this project

**IMPORTANT**: all contributions are required to include some basic tests,
please see [Testing section](#testing)  

For either way you need to know how to export a panel from Bitergia Analytics or GrimoireLab.

## How to export a panel

TBD

# How to submit new panels to existing collections

Once you have a panel (a `json` file) you want to contribute, you need to write some basic documentation about the panel in a Markdown file, and a panel's screenshot in `.jpg` format.

The markdown file must have the following [front matter](https://jekyllrb.com/docs/front-matter/) block:
```
---
title: <panel name>
description: <basic description of the panel, what does it try to solve/show?>
author: <author name>
screenshot: <panel screenshot file name>
created_at: <creation date in YYYY-MM-DD format>
grimoirelab_version: <GrimoireLab platform version in x.x.x format>
layout: panel
---
```

We recommend to use the same meaningful name for these 3 files (json, md and jpg files). Once you have them ready:

1. Fork this project in your own repository
2. Create a branch with the same name of your 3 files. For example:
```
$ git checkout -b <branch name>
```
3. Add the json file to the panels collection folder (the one with all the panels json files).
4. Add the markdown file to the `_docs/<collection>/` folder where it suits.
5. Add the screenshot to the `_docs/images/screenshots/` folder
6. Commit the changes to your branch. For example:
```
$ git commit -a -m "Add <panel name> panel"
```
7. Create a merge request to submit your changes for review

Once reviewed, we will inform if there are changes to be made or if the panel has been accepted.

# How to create a new collection under this project

If you create a set of panels that doesn't fit in any of the existing collections, with their own documentation and screenshots following previous guide, you can add your own collection and contribute it to this project. To do so:

1. Fork this project in your own repository
2. Create a branch with the same name of your 3 files. For example:
```
$ git checkout -b <branch name>
```
3. Create a folder in the root directory with a meaningful name and store in it your panels json files.
4. Create a folder in _docs directory to store your panels markdown files. Add this folder name to `_config.yml` file under `collections` section, and indicate that `output:true` to generate the documentation webpage:
```yaml
collections:
  <folder name>:
    output: true
```
5. Add the screenshots to `_docs/images/screenshots/` folder. Remember to check they are indicated in the markdown front matter, in the `screenshot:` tag.
6. Commit the changes to your branch. For example:
```
$ git commit -a -m "Add <collection name> collection"
```
7. Create a merge request to submit your changes for review

Once reviewed, we will inform if there are changes to be made or if the collection has been accepted.

# Testing

All panels must include some basic tests to make sure which mappings work together to the
index patterns used by the panel. In [test directory](tests/)
you'll find a file named [base.py](tests/base.py) providing the minimum tests you must perform.

In order to add your tests you need to follow these steps:
 * Add a JSON file (or several if needed) containing the mappings against which the tests must
   be executed at [tests/mappings/](tests/mappings).
 * Create a Python file named `test_<panel_name>.py`. E.g: `test_efficiency_timing_overview.py`
 * Copy any existing panel test file, for instance you can use the contents of
   [tests/test_efficiency_timing_overview.py](tests/test_efficiency_timing_overview.py).
 * Modify your test file to include the paths you need to index patterns and mappings files in
  `setUp()` method. See below how it is configured to test `Efficiency: TimingOverview`
   panel:

 ```python
class TestEfficiencyTimingOverview(PanelTest):
    """Tests for Efficiency: TimingOverview panel

    Index Patterns used by the panel: `ocean_tickets`.
    """

    def setUp(self):
        super().setUp()

        # Set mapping file path
        ocean_tickets_mapping = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                             'mappings',
                                             'reference-mapping_a04a4150-18bc-11e9-872f-e17019e68d6d.json')
        # Set index pattern path
        ocean_tickets_index_pattern = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                                   '../open_source_program_office/index-patterns/',
                                                   'index-pattern_a04a4150-18bc-11e9-872f-e17019e68d6d.json')

        # Set the comparison
        self._comparisons[ocean_tickets_index_pattern] = ocean_tickets_mapping

```
 * **NOTICE** you can add as many index pattern vs mapping comparison as you may need to test
   your panel, you are not restricted to use only one as in above's example.
   
 * Don't forget to run `flake8` to check your source code. Repository is already configured
   so you only need to run `flake8 .` from root directory.
 
 * Run your tests and make sure no errors are found. If you have any problem,
   please open an issue and paste your code there.
