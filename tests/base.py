# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Alberto Pérez García-Plaza <alpgarcia@bitergia.com>
#

import json
import unittest

from helpers.model import ESMapping
from helpers.model import IndexPattern


class PanelTest(unittest.TestCase):
    """Tests for index patterns using mapings"""

    def setUp(self):

        self._comparisons = {}

        # Show all diferences when comparing dicts
        # (no ouput limit)
        self.maxDiff = None

    def _load_data(self, index_pattern_path, mapping_path):
        """Load index pattern and mapping from files.

        :param index_pattern_name:
        :param mapping_name:
        :return: index pattern and mapping as `helpers.model` objects.
        """

        with open(index_pattern_path) as f:
            index_pattern_json = json.load(f)
        index_pattern = IndexPattern.from_json(index_pattern_json)

        with open(mapping_path) as f:
            mapping_json = json.load(f)
        es_mapping = ESMapping.from_json(index_name=None,
                                         mapping_json=mapping_json)

        return es_mapping, index_pattern

    def _test_compatibility(self):
        """Test index pattern compatibility with a mapping.

        An index pattern is considered compatible with a
        mapping iif all its fields exists also in the mapping
        and both share same data type. Thus, mapping can have
        more fields than index patterns but not the other way
        around."""

        for index_pattern_name, mapping_name in self._comparisons.items():
            es_mapping, index_pattern = self._load_data(index_pattern_name, mapping_name)

            # Second schema could have more properties than
            # first one (the one used to COMPARE FROM it)
            expected_status = 'OK'
            result = index_pattern.compare_properties(es_mapping)
            self.assertEqual(result['status'], expected_status)

    def _test_strict(self):
        """Test if an index pattern corresponds to a mapping.

        All fields and their types will be checked based on a
        common data model defined by `helpers.model.Schema`.
        Any difference is considered an error.
        """

        for index_pattern_name, mapping_name in self._comparisons.items():
            es_mapping, index_pattern = self._load_data(index_pattern_name, mapping_name)

            # Strict comparison, both must have same properties
            self.assertDictEqual(index_pattern.get_properties(),
                                 es_mapping.get_properties())
