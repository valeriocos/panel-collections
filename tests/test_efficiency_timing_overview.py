# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Alberto Pérez García-Plaza <alpgarcia@bitergia.com>
#

import os
import unittest

from base import PanelTest


class TestEfficiencyTimingOverview(PanelTest):
    """Tests for Efficiency: TimingOverview panel

    Index Patterns used by the panel: `ocean_tickets`.
    """

    def setUp(self):
        super().setUp()

        # Set mapping file path
        ocean_tickets_mapping = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                             'mappings',
                                             'reference-mapping_a04a4150-18bc-11e9-872f-e17019e68d6d.json')
        # Set index pattern path
        ocean_tickets_index_pattern = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                                   '../open_source_program_office/index-patterns/',
                                                   'index-pattern_a04a4150-18bc-11e9-872f-e17019e68d6d.json')

        # Set the comparison
        self._comparisons[ocean_tickets_index_pattern] = ocean_tickets_mapping

    def test_compatibility(self):
        """Test if an index pattern corresponds to a mapping.

        All fields and their types will be checked based on a
        common data model defined by `helpers.model.Schema`.
        Any difference is considered an error.
        """

        super()._test_compatibility()

    def test_strict(self):
        """Test if an index pattern corresponds to a mapping.

        All fields and their types will be checked based on a
        common data model defined by `helpers.model.Schema`.
        Any difference is considered an error.
        """

        super()._test_strict()


if __name__ == '__main__':
    unittest.main()
